package com.shtohryn.task01_Fibonacci;

import java.util.*;

/**
 * @author Pavlo Shtohryn
 * There are methods for working with Fibonacci numbers in this class
 */
public class FibonacciEngine {
    private static final int MOD = (int) (1e7 + 9);// e
    private ArrayList<Integer> fibonacciList = new ArrayList<>();
    /**
     * Method genering fibonacci numbers, with using long arithmetic for get real big variables
     *
     * @param "n" length of fibonacci serial
     */
    public void fibonacciGenerating(int n) {
        int var1 = 1;
        int var2 = 0;
        int res = 0;
        for (int i = 0; i < n; i++) {
            res = (var1 + var2) % MOD;
            fibonacciList.add(res);
            var1 = var2;
            var2 = res;
        }
    }

    /**
     * This method print fibonacci numbers
     * @return
     */
    public ArrayList<Integer> print() {
        return fibonacciList;
    }

    /**
     *Find max odds number in list with numbers
     * @return max odds variable
     * */
    public int maxOddsNumber() {
        int f1 = 0;
        for (int i = fibonacciList.size() - 1; i > 0; i--) {
            if (fibonacciList.get(i) % 2 != 0) {
                f1 = fibonacciList.get(i);
                break;
            }
        }
        return f1;
    }

    /**
     *Find max evens number in list with numbers
     * @return max evens variable
     * */
    public int maxEvensNumber() {
        int f1 = 0;
        for (int i = fibonacciList.size() - 1; i > 0; i--)
            if (fibonacciList.get(i) % 2 == 0) {
                f1 = fibonacciList.get(i);
                break;
            }
        return f1;
    }

    /**
     * Find  percentage Of dds fibonacci numbers
     * @return percentage percentage Of dds fibonacci numbers
     */
    public double percentageOfOddsFibonacciNumbers() {
        int count = 0;
        double percentage = 0;
        if (fibonacciList.size() > 0) {
            for (int i = 0; i < fibonacciList.size(); i++) {
                if (fibonacciList.get(i) % 2 == 0) {
                    count++;
                }
            }
            percentage = 100 * count / fibonacciList.size();

        }
        return percentage;
    }

    /**
     * Find  percentage Of evens fibonacci numbers
     * @return percentage percentage Of evens fibonacci numbers
     */
    public double percentageOfEvenFibonacciNumbers() {
        int count = 0;
        double percentage = 0;
        if (fibonacciList.size() > 0) {
            for (int i = 0; i < fibonacciList.size(); i++) {
                if (fibonacciList.get(i) % 2 != 0) {
                    count++;
                }
            }
            percentage = 100 * count / fibonacciList.size();

        }
        return percentage;
    }
}
