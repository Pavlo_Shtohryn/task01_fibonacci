package com.shtohryn.task01_Fibonacci;

import java.util.*;

/**
 * @author Pavlo Shtohryn
 * The main class with interface menu for user
 */
public class Fibonacci {
    static Scanner scanner = new Scanner(System.in);

    /**
     * In main function user can choose menu with operation with ordinary or fibonacci numbers
     *
     * @param args
     */
    public static void main(String[] args) {
        loading();
        NumbersAnalyser numbersAnalyser = new NumbersAnalyser(0, 0);
        println("----------------Main menu-------------------");
        println("Press -1- work with ordinary variables ");
        println("Press -2- work with fibonacci variables");
        println("Press -0- to finish work");
        while (scanner.hasNext()) {
            menuBar(scanner.nextInt());
        }
    }

    /**
     * This function  consists to bar menu
     *
     * @param choice- number from checking user's choice in bar menu
     */

    private static void menuBar(int choice) {
        switch (choice) {
            case 1:
                loading();
                NumbersAnalyser numbersAnalyser = new NumbersAnalyser(0, 0);
                println("Press -1- to get evens variables between a and b ");
                println("Press -2- to get odds variables between a and b ");
                println("Press -3- to sort evens,from biggest number");
                println("Press -4- to sort odds,from biggest number");
                println("Press -5- sum of all evens variables");
                println("Press -6- sum of all odds variables");
                println("Press -q- for return to " + "MAIN MENU");
                println("Press -0-  to fihish work");
                do {
                    String line = scanner.nextLine();
                    switch (line) {
                        case "1":
                            print("a= ");
                            int a = scanner.nextInt();
                            numbersAnalyser.setA(a);
                            print("b= ");
                            int b = scanner.nextInt();
                            numbersAnalyser.setA(b);
                            loading();
                            println(numbersAnalyser.evenNumbers(a, b));
                            break;
                        case "2":
                            println("a= ");
                            a = scanner.nextInt();
                            println("b= ");
                            b = scanner.nextInt();
                            loading();
                            println(numbersAnalyser.oddNumbers(a, b));
                            break;
                        case "3":
                            loading();
                            println(numbersAnalyser.sortedByBiggestVariable(numbersAnalyser.evenNumbers(numbersAnalyser.getA(), numbersAnalyser.getB())));
                            break;
                        case "4":
                            loading();
                            println(numbersAnalyser.sortedByBiggestVariable(numbersAnalyser.oddNumbers(numbersAnalyser.getA(), numbersAnalyser.getB())));
                            break;
                        case "5":
                            loading();
                            println(numbersAnalyser.evensSum());
                            break;
                        case "6":
                            loading();
                            println(numbersAnalyser.oddsSum());
                        case "q":
                            println("-----------Main menu-------------------");
                            println("Press -1- work with ordinary variables ");
                            println("Press -2- work with fibonacci variables");
                            println("Press -0- to finish work");
                            menuBar(scanner.nextInt());
                            break;
                        case "0":
                            System.exit(0);
                        default:
                            loading();
                            break;
                    }
                } while (scanner.hasNext());
                break;
            case 2:
                loading();
                FibonacciEngine fibonacciEngine = new FibonacciEngine();
                do {
                    println("-------------------FIBONACCI------------------------------------");
                    println("Press -1- to get fibonacci numbers ");
                    println("Press -2- to get percentage of odds fibonacci numbers ");
                    println("Press -3- to get percentage of evens fibonacci numbers ");
                    println("Press -4- max even variable");
                    println("Press -5- max odd variable");
                    println("Press -q- for return to MAIN MENU");
                    println("Press -0-  to finish work");
                    String choiceFib = scanner.nextLine();
                    switch (choiceFib) {
                        case "1":
                            println("Please enter the number of serial");
                            int n = scanner.nextInt();
                            fibonacciEngine.fibonacciGenerating(n);
                            println(fibonacciEngine.print());
                            break;
                        case "2":
                            println(fibonacciEngine.percentageOfOddsFibonacciNumbers());
                            break;
                        case "3":
                            println(fibonacciEngine.percentageOfEvenFibonacciNumbers());
                            break;
                        case "4":
                            println(fibonacciEngine.maxEvensNumber());
                            break;
                        case "5":
                            println(fibonacciEngine.maxOddsNumber());
                            break;
                        case "q":
                            loading();
                            println("-----------Main menu-------------------");
                            println("Press -1- work with ordinary variables ");
                            println("Press -2- work with fibonacci variables");
                            println("Press -0- to finish work");
                            menuBar(scanner.nextInt());
                            break;
                        case "0":
                            System.exit(0);
                        default:
                            loading();
                    }
                } while (scanner.hasNext());
                break;
            case 0:
                System.exit(0);
            default:
                loading();
        }
    }

    /**
     * loading method
     */
    public static void loading() {
        int showProgress = 0;
        String amin1 = "|/-\\";
        String amin2 = "=====================";
        int i = 0;
        println("Loading");
        while (showProgress <= amin2.length()) {
            print("\r " + amin1.charAt(i++ % amin1.length()) + " " + " " +
                    amin2.substring(0, i++ % amin2.length()) + " ");
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            showProgress++;
        }
        print(amin2);
        println(" ");
    }

    /**
     * Method to simpling System.out.print
     * @param o
     */
    public static void print(Object o) {
        System.out.print(o);
    }

    /**
     * Method to simpling System.out.println
     * @param o
     */

    public static void println(Object o) {
        System.out.println(o);
    }
}



