package com.shtohryn.task01_Fibonacci;

import java.util.*;

/**
 * @author Pavlo Shtohryn
 * In this class, there methods for work with ordinary numbers
 */

public class NumbersAnalyser {
    private int a;
    private int b;
    ArrayList<Integer> evensList = new ArrayList<Integer>();
    ArrayList<Integer> oddsList = new ArrayList<Integer>();

    /**
     * Getters and Setters
     */
    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public NumbersAnalyser(int a, int b) {
        this.a = a;
        this.b = b;
    }

    /**
     * The method find evens numbers between to variables, doesn't matters what is biggest
     *
     * @param a - first variable
     * @param b - second variable
     * @return list of evens numbers, between a and b
     */
    public ArrayList<Integer> evenNumbers(int a, int b) {
        if (a == b) {
            if (a % 2 == 0) {
                System.out.println(a);
            } else {
                System.out.println("There are no even variables");
            }
        }
        int less = a < b ? a : b;
        int over = a > b ? a : b;
        int length = over - less;
        for (int i = 0; i < length; i++) {
            less++;
            if (less % 2 == 0)
                evensList.add(less);
        }
        return (evensList);
    }

    /**
     * The method find odds numbers between to variables, doesn't matters what is biggest
     *
     * @param a - first variable
     * @param b - second variable
     * @return list of odds numbers, between a and b
     */
    public ArrayList<Integer> oddNumbers(int a, int b) {
        if (a == b) {
            if (a % 2 == 0) {
                System.out.println(a);
            } else {
                System.out.println("There are no even variables");
            }
        }
        int less = a < b ? a : b;
        int over = a > b ? a : b;
        int length = over - less;
        for (int i = 0; i < length; i++) {
            less++;
            if (less % 2 != 0)
                oddsList.add(less);
        }
        return (oddsList);
    }

    /**
     * @param list with numbers
     * @return sorted list from biggest number
     */
    public ArrayList<Integer> sortedByBiggestVariable(ArrayList<Integer> list) {
        Collections.reverse(list);
        return (list);
    }

    /**
     * @return sum of  evens numbers
     */
    public int evensSum() {
        int sum = 0;
        for (int i = 1; i < evensList.size(); i++)
            sum += evensList.get(i);
        return sum;
    }

    /**
     * @return sum of odds numbers
     */
    public int oddsSum() {
        int sum = 0;
        for (int i = 1; i < oddsList.size(); i++)
            sum += oddsList.get(i);
        return sum;
    }
}
